FROM rust:1.31

RUN apt-get update
RUN apt-get install -y git
RUN mkdir /toolchain
WORKDIR /toolchain
RUN git clone https://github.com/FreeRTOS/FreeRTOS-Kernel.git

RUN rustup default nightly
RUN rustup toolchain install nightly-gnu
RUN rustup default nightly-gnu

RUN git clone https://github.com/lobaro/FreeRTOS-rust
WORKDIR /toolchain/FreeRTOS-rust/freertos-rust-examples

RUN cargo build --example linux
